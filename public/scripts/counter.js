/**
 * Created by Bojan on 3/14/2016.
 */

var Button = React.createClass({
    render: function () {
        return <button onClick={this.handleClick}>+1</button>;
    },
    handleClick: function () {
        this.props.handleClick();
    }
});

var Result = React.createClass({
    render: function () {
        return <div>{this.props.counter}</div>;
    }
});

var Main = React.createClass({
    getInitialState: function () {
        return {counter: 0};
    },
    render: function () {
        return <div>
            <Button handleClick={this.handleClick}/>
            <Result counter={this.state.counter}/>
        </div>;
    },
    handleClick: function () {
        this.setState(function (state) {
            return {counter: state.counter + 1};
        });
    }
});


ReactDOM.render(
    <Main />,
    document.getElementById('container')
);


