/**
 * Created by Bojan on 3/14/2016.
 */

var Email = React.createClass({
    render: function(){
        return <input type="email" onChange={this.handleChange} placeholder="Email..." />;
    },
    handleChange: function(e){
        this.props.handleChange(e.target.value);
    }
});

var Button = React.createClass({
    render: function(){
        return <button disabled={!this.props.validEmail}>Am I Evil</button>;
    }
});

var Validation = React.createClass({
    render: function(){
        return <div>
            <Email handleChange={this.handleEmailChange}/>
            <Button validEmail={this.state.validEmail} />
            <Volume />
        </div>
    },
    getInitialState: function () {
        return {validEmail: false};
    },
    handleEmailChange: function(email){
        if(validateEmail(email)){
            this.setState(function () {return {validEmail: true}});
        }
        else {
            this.setState(function () {return {validEmail: false}});
        }
    }
});

var Volume = React.createClass({
    getInitialState: function(){
        return {
            value: 10
        };
    },
    handleChange: function(value) {
        this.setState({
            value: value
        });
    },
    render: function() {
        return (
            <Slider
                value={value}
                orientation="vertical"
                onChange={this.handleChange} />
        );
    }
});


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

ReactDOM.render(<Validation/>, document.getElementById("validation"));